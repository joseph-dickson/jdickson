<?php
/** 
 * Query Posts for featured content
 * 
 */

/** 
 * Add a counter used in class tags featured-1, featured-2, featured-3
 * Used by CSS to alternate between left and right alignment of image
 * with content alongside.
 *
 */
$counter = 0;   

$the_query = new WP_Query( array( 
	'post_type' => 'featured-content',
	'order' => 'ASC',
	'posts_per_page' => 3,
) );
	
// The Loop
if ( $the_query->have_posts() ) {

	while ( $the_query->have_posts() ) {

		$the_query->the_post();

    		$counter++; // Add one to counter
?>
<div class="featured-tiles">
	<div <?php post_class( array( 'columns', 'small-12', 'featured-' . $counter ) ); ?>>
	<article id="post-<?php the_ID(); ?>">
			<?php

				// check if Advanced Custom Fields redirect is set
				$redirect = get_field( 'redirect_to_url' );
				if ( '' != $redirect ) {
					echo '<a href="' . $redirect . '">';

					if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'featured-content' );
					}
			?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php
				the_content();
					echo '</a>'; // close get_permalink hyperlink
				}
			?>

			<?php	// Edit link added to end of post

				get_template_part( 'template-parts/edit-post-link' ); 

			?>
		</article>
	</div>
</div>
<?php
// End while loop for 'event-image'
	}
}
