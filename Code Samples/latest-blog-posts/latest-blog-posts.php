<?php
/*
 *  Query the three latest blog posts 
 */
$the_query = new WP_Query( array( 
	'post_type' => 'post',
	'order' => 'DESC',
	'posts_per_page' => 3,
) );
	
// The Loop
if ( $the_query->have_posts() ) {

	echo '<div class="row">';

	while ( $the_query->have_posts() ) {

		$the_query->the_post();
?>
<div class="events">
	<div <?php post_class( array( 'event', 'columns', 'small-12', 'medium-4' ) ); ?>>
		<article id="post-<?php the_ID(); ?>">
			<?php 
				echo '<a href="' . get_permalink() . '">';

					if ( has_post_thumbnail() ) {
						the_post_thumbnail( 'featured-content' );
					}
			?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php
				the_excerpt();
				echo '</a>'; // close get_permalink hyperlink
			?>

			<?php	// Edit link added to end of post

				get_template_part( 'template-parts/edit-post-link' ); 

			?>
		</article>
	</div>
</div>
<?php
// End while loop for 'event-image'
	}

	echo '</div>';

}
