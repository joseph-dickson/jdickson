<?php
$current_post_ID = get_the_ID(); // the post's id is assigned to $current_post_ID


$args = array(
	'post_type' => 'post',
	'orderby' => 'date',
	'order'   => 'DESC',
	'category_name' => 'covid-19-messages' ,
	'post_type' => 'post', 'post__not_in' => array( $current_post_ID ),
);

// the query
$the_query = new WP_Query( $args );
?>

<?php if ( $the_query->have_posts() ) {

	$category = get_cat_name( 8 ); // Get Category by ID number.

	echo '<h2><strong>' . $category . '</strong></h2>';

	echo '<div class="accordion" data-accordion>';

	while ( $the_query->have_posts() ) : $the_query->the_post();

			echo '<article class="accordion-item" data-accordion-item>';

				the_title( '<a href="#" class="accordion-title">', ' - <span>' .  get_the_date('m.d.y') . '</span></a>' );

				echo '<div class="accordion-content" data-tab-content>';

					echo '<a href="' . get_permalink() . '"><span class="dashicons dashicons-admin-links"></span></a>';

					the_content();

					get_template_part( 'template-parts/edit-post-link' );

				echo '</div>';

			echo '</article>';

	endwhile;

	echo '</div>';

} else { 
//	_e( 'Sorry, no posts matched your criteria.' );
}

wp_reset_postdata();
