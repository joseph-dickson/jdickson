<?php
// Check if Advanced Custom Fields is installed.
if ( function_exists( 'get_field') ) {
	$args = array(
		'post_type' => array( 'featured-tile' ),
	);

	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
	
		$the_query->the_post();

			if ( has_post_thumbnail() ) {

				// check if redirect is set
				$redirect = get_field('redirect_to_url');
				if ( '' != $redirect ) {
					echo '<a href="' . $redirect . '">';
				}
		
					the_post_thumbnail( 'medium' );

				if ( '' != $redirect ) {
					echo '</a>';
				}

			}

	    	}
	} else {
	    // no posts found
	}

	// Restore original Post Data
	wp_reset_postdata();

} else {
 	// Please install ACF Plugin
 echo 'Please install and activate the ACF plugin';
}
