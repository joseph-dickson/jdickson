<?php
// Display "Hero Image" Custom Post Type
$args = array(
	'post_type' => 'hero-image',
);

// The Query
$the_query = new WP_Query( $args );
 
// The Loop
if ( $the_query->have_posts() ) {

	while ( $the_query->have_posts() ) {

		$the_query->the_post();

?>


<article id="post-<?php the_ID(); ?>" <?php post_class('hero-image'); ?>>

<?php
	$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero-image' ); 
	// check if Advanced Custom Fields redirect is set
	$redirect = get_field('redirect_to_url');
		if ( '' != $redirect ) {
		echo '<a href="' . $redirect . '">';
?>

	<div style="background-image: url('<?php echo $backgroundImg[0] ?>'); background-position: center center; background-size: contain; background-repeat: no-repeat; min-height: 75vh">
		<div class="opacity-overlay">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
			</header>
		</div>
	</div>

<?php } ?>

</article><!-- #post-## -->

<?php	// Edit link added to end of post
	get_template_part( 'template-parts/edit-post-link' ); 

?>

<?php
// End while loop for 'hero-image'
	}

} 
/* Restore original Post Data */
wp_reset_postdata();
?>

