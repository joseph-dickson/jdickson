<?php
/**
 * Plugin Name: PZ Custom Post Types
 * Description: Custom Post Type for the Zest Newsletter
 * Version: 0.1
 * License: GPL2
 */

// Prevent direct access to this file
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Create a custom post type for Zest News and Events
 * An "schedule" icon will apear in dashboard navigation
 * A custom zest slug permalink is created
 *
 * https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 */
function pz_custom_post_types() {


    $labels = array(
        'name'			=> 'Zest',
        'singular_name'		=> 'Zest',
        'menu_name'		=> 'Zest',
        'name_admin_bar'	=> 'Zest Newsletter',
        'add_new'		=> 'Add New Newsletter',
        'add_new_item'		=> 'Add New Newsletter',
        'new_item'		=> 'New Zest',
        'edit_item'		=> 'Edit Zest',
        'view_item'		=> 'View Zest',
        'all_items'		=> 'All Zest',
        'search_items'		=> 'Search Zest',
        'parent_item_colon'	=> 'Parent Zest:',
        'not_found'		=> 'No Zest newsletters found.',
        'not_found_in_trash'	=> 'No Zest newsletters found in Trash.',
    );

    $args = array(
        'labels'		=> $labels,
        'public'		=> true,
        'publicly_queryable'	=> true,
        'show_ui'		=> true,
        'show_in_menu'		=> true,
        'menu_icon'		=> 'dashicons-schedule',
        'query_var'		=> true,
        'rewrite'		=> array( 'slug' => 'zest' ),
        'capability_type'	=> 'post',
        'has_archive'		=> true,
        'hierarchical'		=> false,
	'menu_position'		=> 5,
	'show_in_rest'		=> true,
	'supports'		=> array( 'title', 'editor', 'excerpt' ),
    );

    register_post_type( 'zest', $args );
}
add_action( 'init', 'pz_custom_post_types' );

// Flush rewrite rules to add "Zest" as a permalink slug when plugin is activated
function my_rewrite_flush() {
    pz_custom_post_types();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );
