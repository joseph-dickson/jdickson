<?php
/**
 * Plugin Name: PZ Tile Post Type 
 * Description: Custom Post Type for promotional image tiles
 * Version: 1.0
 * License: GPL2
 */

// Prevent direct access to this file
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 */
function pz_tile_custom_post_type() {

$labels = array(
	'name'			=> 'Featured Tile',
	'singular_name'		=> 'Tile',
	'menu_name'		=> 'Tile',
	'name_admin_bar'	=> 'Tile',
	'add_new'		=> 'Add a Featured Tile',
	'add_new_item'		=> 'Featured Tile',
	'new_item'		=> 'Featured Tile',
	'edit_item'		=> 'Edit Tile',
	'view_item'		=> 'View Tile',
	'all_items'		=> 'All Featured Tiles',
	'search_items'		=> 'Search Featured Tiles',
	'parent_item_colon'	=> 'Parent Featured Tiles:',
	'not_found'		=> 'No tiles found.',
	'not_found_in_trash'	=> 'No tiles found in Trash.',
);

$args = array(
	'labels'		=> $labels,
	'public'		=> false,
	'publicly_queryable'	=> false,
	'show_ui'		=> true,
	'show_in_menu'		=> true,
	'menu_icon'		=> 'dashicons-slides',
	'query_var'		=> true,
	'rewrite'		=> array( 'slug' => 'featured-tile' ),
	'capability_type'	=> 'post',
	'has_archive'		=> true,
	'hierarchical'		=> true,
	'menu_position'		=> 10,
	'show_in_rest'		=> true,
	'supports'		=> array( 'title', 'thumbnail' ),
);

register_post_type( 'featured-tile', $args );
}

add_action( 'init', 'pz_tile_custom_post_type' );

// Flush rewrite rules when plugin is activated
function pz_tile_rewrite_flush() {
	pz_tile_custom_post_type();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'pz_tile_rewrite_flush' );
