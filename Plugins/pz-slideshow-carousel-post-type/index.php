<?php
/**
 * Plugin Name: PZ Slideshow Carsousel
 * Description: Custom Post Type for Slideshow Carousels
 * Version: 1.0
 * License: GPL2
 */

// Prevent direct access to this file
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Create a custom post type for Zest News and Events
 * An "schedule" icon will apear in dashboard navigation
 * A custom zest slug permalink is created
 *
 * https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 */
function pz_slideshow_carousel_custom_post_type() {

/**
 * Hero Images for the Front Page
 * Theme will need to be configured using WP Query to use this tool
 */
$labels = array(
	'name'			=> 'Carousel Slideshow',
	'singular_name'		=> 'Carousel',
	'menu_name'		=> 'Carousel',
	'name_admin_bar'	=> 'Carousel',
	'add_new'		=> 'Add a Carousel Slide',
	'add_new_item'		=> 'Carousel Slide',
	'new_item'		=> 'Carsusel Slide',
	'edit_item'		=> 'Edit Slide',
	'view_item'		=> 'View Slide',
	'all_items'		=> 'All Carousel Slides',
	'search_items'		=> 'Search Carousel Slides',
	'parent_item_colon'	=> 'Parent Carousel Slides:',
	'not_found'		=> 'No slides found.',
	'not_found_in_trash'	=> 'No slides found in Trash.',
);

$args = array(
	'labels'		=> $labels,
	'public'		=> true,
	'publicly_queryable'	=> false,
	'show_ui'		=> true,
	'show_in_menu'		=> true,
	'menu_icon'		=> 'dashicons-format-gallery',
	'query_var'		=> true,
	'rewrite'		=> array( 'slug' => 'carousel' ),
	'capability_type'	=> 'post',
	'has_archive'		=> true,
	'hierarchical'		=> true,
	'menu_position'		=> 10,
	'show_in_rest'		=> true,
	'supports'		=> array( 'title', 'editor', 'thumbnail' ),
);

register_post_type( 'carousel', $args );
}

add_action( 'init', 'pz_slideshow_carousel_custom_post_type' );

if ( function_exists( 'add_theme_support' ) ) {
	add_image_size( 'carousel', 1200, 368, array( 'center', 'center' ) ); // Custom thumbnail size for a carousel slide
}

// Flush rewrite rules when plugin is activated
function my_rewrite_flush() {
	pz_slideshow_carousel_custom_post_type();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );
