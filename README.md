# README #

## What is this repository for? ##

This a public repository of Joseph Dickson's selected WordPress Plugins and Themes for sharing and review. I offer it freely without warranty or support.

License: [GPL 3](https://www.gnu.org/licenses/gpl-3.0.html)
